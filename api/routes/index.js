const routes = (app) => {
    app.get('/', (req, res) => {
        res.send('Welcome to our API')
    })

    app.post('/text', (req, res) => {
        const data = req.body
        res.send(data)
    })
}

export default routes
