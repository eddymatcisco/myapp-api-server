import express from 'express'
import cors from 'cors'
import { createServer } from 'http'
import config from './config/config'
import routes from './routes'
import bodyParser from 'body-parser'

const app = express()

const corsOptions = {
    origin: config.server.CLIENT_URL
}

app.use(cors(corsOptions))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

routes(app)
console.log(config)

const server = createServer(app)
server.listen(config.server.PORT, function () {
    console.log('App running on port', config.server.PORT)
})

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err)
})
