const config = {
    NODE_ENV: process.env.NODE_ENV || 'dev',
    server: {
        SERVER_URL: process.env.SERVER_URL || 'http://localhost:8081',
        CLIENT_URL: process.env.CLIENT_URL || 'http://localhost:3000',
        PORT: process.env.PORT || 8081
    }
}

export default config
