# myApp API Server

Node.js API server built using Docker containers

1. Make sure that you have docker install in your system
2. You can login form the terminal using the command below:

 -- $ docker login -username <your username>
 -- $ Password: #######

3. Let create a docker image on the current directory
 
 -- $ docker build -t myapp-api-image:latest .
 -- $ docker images
 
4. Let build a docker container to run the docker image

 -- $ docker run -it -p 8081:8081 myapp-api-image:latest
 -- $ docker ps
