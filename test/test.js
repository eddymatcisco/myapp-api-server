import { describe, it } from 'mocha'
import chai from 'chai'
import request from 'supertest'
import express from 'express'
import routes from '../api/routes'
import bodyParser from 'body-parser'

const app = express()
const expect = chai.expect

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

routes(app)
describe('API calls', () => {
    it('Run Test', (done) => {
        console.log('Running tests!!!')
        done()
    })
})

describe('POST /text', function() {
    let data = {
        text: 'This is a text test'
    }
    it('Response should be the same text', function (done) {
        request(app)
            .post('/text')
            .send(data)
            .set('Content-Type', 'application/json')
            .end((err, res) => {
                expect(res.statusCode).to.equal(200)
                expect(res.body.text).to.equal(data.text)
                done()
            })
    })
})
